import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/material.dart';

class Product extends Equatable {
  const Product({
    Key? key,
    required this.name,
    required this.category,
    required this.imageUrl,
    required this.price,
    required this.isRecommended,
    required this.isPopular,
  });

  final String name;
  final String category;
  final String imageUrl;
  final double price;
  final bool isRecommended;
  final bool isPopular;

  static Product fromSnapshot(DocumentSnapshot snap) {
    Product product = Product(
      name: snap['name'],
      category: snap['category'],
      imageUrl: snap['imageUrl'],
      price: snap['price'],
      isRecommended: snap['isRecommended'],
      isPopular: snap['isPopular'],
    );

    return product;
  }

  @override
  List<Object?> get props =>
      [name, category, imageUrl, price, isRecommended, isPopular];

  // static List<Product> products = [
  //   const Product(
  //     name: 'Soft Drink #1',
  //     category: 'Soft Drinks',
  //     imageUrl:
  //         'https://images.unsplash.com/photo-1523205771623-e0faa4d2813d?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=89719a0d55dd05e2deae4120227e6efc&auto=format&fit=crop&w=1953&q=80',
  //     price: 2.99,
  //     isRecommended: true,
  //     isPopular: false,
  //   ),
  //   const Product(
  //     name: 'Soft Drink #2',
  //     category: 'Soft Drink',
  //     imageUrl:
  //         'https://images.unsplash.com/photo-1508704019882-f9cf40e475b4?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=8c6e5e3aba713b17aa1fe71ab4f0ae5b&auto=format&fit=crop&w=1352&q=80',
  //     price: 2.99,
  //     isRecommended: false,
  //     isPopular: false,
  //   ),
  //   const Product(
  //     name: 'Soft Drink #1',
  //     category: 'Soft Drink',
  //     imageUrl:
  //         'https://images.unsplash.com/photo-1519985176271-adb1088fa94c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=a0c8d632e977f94e5d312d9893258f59&auto=format&fit=crop&w=1355&q=80',
  //     price: 2.99,
  //     isRecommended: true,
  //     isPopular: false,
  //   ),
  //   const Product(
  //     name: 'Soft Drink #3',
  //     category: 'Soft Drink',
  //     imageUrl:
  //         'https://images.unsplash.com/photo-1586882829491-b81178aa622e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2850&q=80',
  //     price: 2.99,
  //     isRecommended: true,
  //     isPopular: true,
  //   ),
  //   const Product(
  //     name: 'Smoothies #1',
  //     category: 'Smoothies',
  //     imageUrl:
  //         'https://images.unsplash.com/photo-1586871608370-4adee64d1794?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2862&q=80',
  //     price: 2.99,
  //     isRecommended: true,
  //     isPopular: false,
  //   ),
  //   const Product(
  //     name: 'Smoothies #2',
  //     category: 'Smoothies',
  //     imageUrl:
  //         'https://images.unsplash.com/photo-1586901533048-0e856dff2c0d?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1650&q=80',
  //     price: 2.99,
  //     isRecommended: false,
  //     isPopular: false,
  //   ),
  // ];
}
