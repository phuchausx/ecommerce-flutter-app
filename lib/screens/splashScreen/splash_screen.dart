import 'dart:async';

import 'package:flutter/material.dart';
import 'package:flutter_ecommerr/helpers/asset_helper.dart';
import 'package:flutter_ecommerr/helpers/image_helper.dart';

class SplashScreen extends StatelessWidget {
  static const String routeName = '/splash';

  static Route route() {
    return MaterialPageRoute(
      settings: RouteSettings(name: routeName),
      builder: (_) => SplashScreen(),
    );
  }

  @override
  Widget build(BuildContext context) {
    Timer(Duration(seconds: 3), () => Navigator.pushNamed(context, '/'));
    return Scaffold(
        body: Container(
      child: ImageHelper.loadFromAsset(
        AssetHelper.imageNike,
        fit: BoxFit.cover,
        height: double.infinity,
        width: double.infinity,
      ),
    ));
  }
}
