import 'package:flutter/material.dart';
import 'package:flutter_ecommerr/models/models.dart';
import 'package:flutter_ecommerr/screens/screens.dart';
import 'package:flutter_ecommerr/screens/splashScreen/splash_screen.dart';

class AppRouter {
  static Route onGenerateRoute(RouteSettings settings) {
    {
      switch (settings.name) {
        case '/':
          return HomeScreen.route();
        case HomeScreen.routeName:
          return HomeScreen.route();
        case CartScreen.routeName:
          return CartScreen.route();
        case CatalogScreen.routeName:
          return CatalogScreen.route(category: settings.arguments as Category);
        case ProductScreen.routeName:
          return ProductScreen.route(product: settings.arguments as Product);
        case SplashScreen.routeName:
          return SplashScreen.route();
        case WishlistScreen.routeName:
          return WishlistScreen.route();
        case CheckoutScreen.routeName:
          return CheckoutScreen.route();
        default:
          return _errorRoute();
      }
    }
  }

  static Route _errorRoute() {
    return MaterialPageRoute(
      settings: RouteSettings(name: '/error'),
      builder: (_) => Scaffold(
        appBar: AppBar(
          title: Text('Error'),
        ),
      ),
    );
  }
}
