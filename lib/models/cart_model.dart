import 'package:equatable/equatable.dart';
import 'package:flutter_ecommerr/models/models.dart';

class Cart extends Equatable {
  const Cart({this.products = const <Product>[]});

  final List<Product> products;

  double get subtotal =>
      products.fold(0, (total, current) => total + current.price);

  double deliveryFee(subtotal) {
    if (subtotal >= 30.0) {
      return 0.0;
    } else {
      return 10.0;
    }
  }

  double total(subtotal, deliveryFee) {
    return subtotal + deliveryFee;
  }

  String freeDelivery(subtotal) {
    if (subtotal >= 30.0) {
      return 'You have Free Delivery';
    } else {
      double missing = 30.0 - subtotal;
      return 'Add \$${missing.toStringAsFixed(2)} for FREE Delivery';
    }
  }

  String get subtotalString => subtotal.toStringAsFixed(2);
  String get totalString =>
      total(subtotal, deliveryFee(subtotal)).toStringAsFixed(2);
  String get deliveryFeeString => deliveryFee(subtotal).toStringAsFixed(2);
  String get freeDeliveryString => freeDelivery(subtotal);

  // ý tưởng là sẽ tạo ra 1 mảng type Map gồm key và value
  // đầu tiên tạo mảng kiểu Map là quantity rỗng
  // Lần lượt đi qua từng phần tử của mảng products . Chúng ta sẽ lấy từng phần tử đó và check
  //    xem đã chứa key đó chưa.
  // Trường hợp chưa có key tồn tại trong mảng quantity thì đẩy vào với key là object product và
  //    giá trị value là 1 (Tương đương với số lượng ban đầu sẽ là 1)
  // Trường hợp nếu có rồi thì sẽ cộng giá trị đó với key đó lên 1 (Tương đương với tăng số lượng product lên 1 đơn vị)
  Map productQuantity(products) {
    var quantity = Map();

    products.forEach((product) {
      if (!quantity.containsKey(product)) {
        quantity[product] = 1;
      } else {
        quantity[product] += 1;
      }
    });

    return quantity;
  }

  @override
  List<Object?> get props => [products];
}
