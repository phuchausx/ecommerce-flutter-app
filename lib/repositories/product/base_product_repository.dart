import 'package:flutter_ecommerr/models/models.dart';

abstract class BaseProductRepository {
  Stream<List<Product>> getAllProducts();
}
