import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter_ecommerr/models/models.dart';
import 'package:flutter_ecommerr/repositories/categpry/base_category_repository.dart';

class CategoryRepository extends BaseCategoryRepository {
  CategoryRepository({FirebaseFirestore? firebaseFirestore})
      : _firebaseFirestore = firebaseFirestore ?? FirebaseFirestore.instance;

  final FirebaseFirestore _firebaseFirestore;

  @override
  Stream<List<Category>> getAllCategories() {
    return _firebaseFirestore
        .collection('categories')
        .snapshots()
        .map((snapshot) {
      return snapshot.docs.map((doc) => Category.fromSnapshot(doc)).toList();
    });
  }
}
